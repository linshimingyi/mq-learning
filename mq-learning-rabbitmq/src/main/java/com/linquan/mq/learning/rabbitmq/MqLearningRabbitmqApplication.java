package com.linquan.mq.learning.rabbitmq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MqLearningRabbitmqApplication {

    public static void main(String[] args) {
        SpringApplication.run(MqLearningRabbitmqApplication.class, args);
    }

}
