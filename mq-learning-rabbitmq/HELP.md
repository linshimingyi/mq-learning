概念
    1, 消息（Message）
        |-- label 包括routing_key priority(消息优先权) delivery-mode(持久性存储)
        |-- body
    2, 生产者 消息发送者
    3, 交换器（Exchange）接受消息并将消息路由给队列，如果路由不到则返回给生产者或直接丢弃（视配置而定）
    4, 绑定（Binding）通过BindingKey将交换器和队列绑定
    5, 路由键（RoutingKey）用来指定消息的路由规则
    6, 队列（Queue）用来保存消息知道发送给消费者
    7, 连接（Connection）网络连接，比如一个TCP连接
    8, 信道（Channel）多路复用连接中的一条独立的双向数据流通道。信道是建立在真实的TCP连接内的虚拟连接，
                     AMQP命令都是通过信道发出去的，不管是发布消息、订阅队列还是接收消息，这些动作都
                     是通过信道完成的。因为对于操作系统来说建立和销毁TCP都是非常昂贵的开销，所以引入
                     了信道的概念，以复用一条TCP连接。
    9, 消费者（Consumer）消息消费者
    10, 虚拟主机（Virtual Host）虚拟主机，表示一批交换器、消息队列和相关对象。虚拟主机是共享相同的身份
                              认证和加密环境的独立服务器域。每个vhost本质上就是一个mini版的RabbitMQ
                              服务器，拥有自己的队列、交换器、绑定和权限机制。vhost是AMQP概念的基础，
                              必须在连接时指定，RabbitMQ默认的vhost是/。
    11, 服务节点（Broker）对于RabbitMQ来说，一个RabbitMQBroker可以简单地看作一个RabbitMQ服务节点，
                        或者RabbitMQ服务实例。大多数情况下也可以将一个RabbitMQ Broker看作一台
                        RabbitMQ服务器。

交换器（Exchange）类型
    1, fanout-- 把消息路由到所有与该交换器绑定的队列中
    2, direct-- 把消息路由到BindingKey和RoutingKey完全匹配的队列中
    3, topic -- 把消息根据路由规则路由到相应的队列中

RabbitMQ运转流程
生产者端：
    1, 生产者连接到RabbitMQ Broker，建立一个连接（Connection），开启一个信道（Channel）
    2, 生产者声明一个交换器，并设置相关属性，比如交换机类型、是否持久化等
    3, 生产者声明一个队列并设置相关属性，比如是否排他，是否持久化，是否自动删除等
    4, 生产者通过路由键将交换器和队列绑定起来
    5, 身缠这发送消息至RabbitMQ Broker，其中包含路由键、交换器等信息
    6, 相应的交换器根据接收到的路由键查找相匹配的队列
    7, 相应的交换器根据接收到的路由键查找相匹配的队列
    8, 如果找到，则将从生产者发送过来的消息存入相应的队列中
    9, 如果没找到，则根据生产者配置的属性选择丢弃还是回退给生产者
    10, 关闭信道
    11, 关闭连接
消费者端：

ref: 
https://www.jianshu.com/p/377a68ce6744
https://blog.csdn.net/qq_35387940/article/details/100514134