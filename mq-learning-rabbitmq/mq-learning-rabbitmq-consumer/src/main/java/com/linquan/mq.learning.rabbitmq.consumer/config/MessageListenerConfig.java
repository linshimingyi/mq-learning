package com.linquan.mq.learning.rabbitmq.consumer.config;

import com.linquan.mq.learning.rabbitmq.consumer.receiver.DirectReceiver;
import com.linquan.mq.learning.rabbitmq.consumer.receiver.FanoutReceiverA;
import org.springframework.amqp.core.AcknowledgeMode;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author linshimingyi
 * @package com.linquan.mq.learning.rabbitmq.consumer.config
 * @date 2019/10/16 00:06
 */
@Configuration
public class MessageListenerConfig {

    @Autowired
    private CachingConnectionFactory factory;

    @Autowired
    private DirectReceiver directReceiver;

    @Autowired
    private FanoutReceiverA fanoutReceiverA;

    @Autowired
    private DirectRabbitMqConfig directRabbitMqConfig;

    @Autowired
    private FanoutRabbitConfig fanoutRabbitConfig;

    @Bean
    public SimpleMessageListenerContainer simpleMessageListenerContainer() {
        SimpleMessageListenerContainer container = new SimpleMessageListenerContainer(factory);
        container.setConcurrentConsumers(1);
        container.setMaxConcurrentConsumers(1);
        container.setAcknowledgeMode(AcknowledgeMode.MANUAL);
        container.setQueues(directRabbitMqConfig.testDirectQueue());
        container.setMessageListener(directReceiver);
        container.addQueues(fanoutRabbitConfig.queueA());
        container.setMessageListener(fanoutReceiverA);
        return container;
    }
}
