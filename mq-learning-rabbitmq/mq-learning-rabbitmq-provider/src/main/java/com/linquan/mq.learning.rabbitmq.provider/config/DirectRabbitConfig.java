package com.linquan.mq.learning.rabbitmq.provider.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DirectRabbitConfig {

    /**
     * 队列，起名TestDirectQueue true表示持久化
     *
     * @return
     */
    @Bean
    public Queue testDirectQueue() {
        return new Queue("TestDirectQueue", true);
    }

    /**
     * direct交换机 起名TestDirectExchange
     *
     * @return
     */
    @Bean
    public DirectExchange testDirectExchange() {
        return new DirectExchange("TestDirectExchange");
    }

    /**
     * 绑定 将队列和交换机绑定 并设置用于匹配键 TestDirectRouting
     * @return
     */
    @Bean
    public Binding bindingDirect() {
        return BindingBuilder.bind(testDirectQueue()).to(testDirectExchange()).with("TestDirectRouting");
    }

    @Bean
    DirectExchange lonelyDirectExchange() {
        return new DirectExchange("lonelyDirectExchange");
    }
}
