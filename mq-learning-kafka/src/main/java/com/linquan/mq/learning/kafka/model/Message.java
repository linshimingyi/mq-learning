package com.linquan.mq.learning.kafka.model;

import lombok.Data;

import java.time.LocalDateTime;

/**
 * @Author zhaoyu04
 * @Date 2019/10/24 5:01 下午
 */
@Data
public class Message {

    private Long id;

    private String msg;

    private LocalDateTime sendTime;
}
