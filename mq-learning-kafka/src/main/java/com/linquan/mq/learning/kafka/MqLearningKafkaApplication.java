package com.linquan.mq.learning.kafka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MqLearningKafkaApplication {

    public static void main(String[] args) {
        SpringApplication.run(MqLearningKafkaApplication.class, args);
    }

}
