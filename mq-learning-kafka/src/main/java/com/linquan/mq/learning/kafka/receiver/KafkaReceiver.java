package com.linquan.mq.learning.kafka.receiver;

import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

/**
 * @Author zhaoyu04
 * @Date 2019/10/24 5:29 下午
 */
@Component
@Slf4j
public class KafkaReceiver {

    @KafkaListener(topics = {"mq-learning-kafka"})
    public void receive(ConsumerRecord<?, ?> record) {
        log.info("消费得到的消息---key: {}, 消费得到的消息---value: {}", record.key(), record.value().toString());
    }
}
