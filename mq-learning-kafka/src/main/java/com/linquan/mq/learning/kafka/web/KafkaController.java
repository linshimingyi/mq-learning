package com.linquan.mq.learning.kafka.web;

import com.alibaba.fastjson.JSONObject;
import com.linquan.mq.learning.kafka.model.Message;
import com.linquan.mq.learning.kafka.sender.KafkaSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.Random;

/**
 * @Author zhaoyu04
 * @Date 2019/10/24 5:09 下午
 */
@RestController
public class KafkaController {

    @Autowired
    private KafkaSender sender;

    @GetMapping("/sendMsgToKafka")
    public String sendMsgToKafka() {
        Message message = new Message();
        message.setId(new Random().nextLong());
        message.setMsg("我有一头小毛驴");
        message.setSendTime(LocalDateTime.now());
        sender.send("mq-learning-kafka", JSONObject.toJSONString(message));

        return "hi guy!";
    }
}
