概念
1，broker
   服务节点。broker存储topic的数据。
   如果某topic有N个partition，集群有N个broker，那么每个broker存储该topic的一个partition；
   如果某topic有N个partition，集群有N+M个broker，那么其中N个broker存储该topic的一个partition，
   剩下的M个broker不存储该topic的partition数据；
   如果某topic有N个partition，集群中broker数目少于N个，那么一个broker存储该topic的一个或多个partition。
   在实际生产环境，应尽量避免这种。
2，topic
   每条发布到kafka集群的消息都有一个类别，这个类别被称为topic。物理上，不同topic的消息分开存储，逻辑上一个topic
   的消息虽然保存于一个或者多个broker上，但用户只需指定消息的topic即可生产或者消费数据而不必关心数据存于何处  
3，partition
   topic中的数据分割为一个或者多个partition。每个topic至少有一个partition。每个partition中的数据使用多个
   segment文件存储。partition中的数据是有序的，不同partition间的数据丢失了数据的顺序。如果topic有多个partition，
   消费数据时就不能保证数据的顺序。在需要严格保证消息的顺序的场景下，需要将partition数目设为1.
4，producer
   生产者，该角色将消息发布到kafka的topic中，broker接收到生产者发送的消息后，broker将该消息追加到当前用于追加
   数据的segment文件中。生产者发送的消息，存储到一个partition中，生产者也可以指定数据存储的partition。
5，consumer
   消费者可以从broker中读取数据。消费者可以消费多个topic中的数据
6，consumer group
   每个consumer属于一个特定的consumer group（可为每个consumer指定group name，若不指定group name则属于默认group）
7 leader
  每个partition有多个副本，其中有且仅有一个座位leader，leader是当前负责数据的读写的partition
8 follower
  follower跟随leader，所有写请求都会通过leader路由，数据变更会广播给所有的follower，follower与leader保持数据
  同步，如果leader失效，则从follower中选举出一个新的leader。当Follower与Leader挂掉、卡住或者同步太慢，leader
  会把这个follower从“in sync replicas”（ISR）列表中删除，重新创建一个Follower。   

kafka命令：
1，启动
   先启动zk:bin/zookeeper-server-start.sh -daemon config/zookeeper.properties
   再启动kafka:bin/kafka-server-start.sh  config/server.properties
2，topic
   创建topic
   bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic test1
   查看topic
   bin/kafka-topics.sh --list --zookeeper localhost:2181
3，生产消息   
   bin/kafka-console-producer.sh --broker-list localhost:9092 --topic test1
4，消费消息
   bin/kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic test1 --from-beginning

ref:
https://www.cnblogs.com/qingyunzong/p/9004509.html
https://blog.csdn.net/qq_35387940/article/details/87969850